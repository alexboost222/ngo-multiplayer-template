using Core.Application;
using Core.UnityExtensions;
using UnityEditor;
using UnityEngine;
using UnityToolbarExtender;
using EnterPlayModeOptions = Core.Application.EnterPlayModeOptions;

namespace Editor
{
    [InitializeOnLoad]
    public class ToolbarEnterPlyModeOption
    {
        static ToolbarEnterPlyModeOption()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
        }

        private static void OnToolbarGUI()
        {

            var loadSetup = EnumEditorPrefs.GetOrDefault(NetworkSetup.Local);
            var setup = (NetworkSetup)EditorGUILayout.EnumPopup(loadSetup);
            if (setup == NetworkSetup.Lobby) Debug.LogError($"NetworkSetup.{NetworkSetup.Lobby} is not allowed!");
            else if (setup != loadSetup) EnumEditorPrefs.Set(setup);


            var loadRole = EnumEditorPrefs.GetOrDefault(NetworkRole.Host);
            var role = (NetworkRole)EditorGUILayout.EnumPopup(loadRole);
            if (role == NetworkRole.Unknown) Debug.LogError($"NetworkRole.{NetworkRole.Unknown} is not allowed!");
            else if (role != loadRole)EnumEditorPrefs.Set(role);
            
            GUILayout.FlexibleSpace();
        }
    }
}