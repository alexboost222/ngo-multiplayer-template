using System.Collections.Generic;
using NaughtyAttributes;
using NaughtyAttributes.Editor;
using UnityEditor;
using UnityEngine;
using UnityExtensions.TagSystem;

namespace Editor
{
    [CustomPropertyDrawer(typeof(ProjectTagAttribute))]
    public class ProjectTagPropertyDrawer : PropertyDrawerBase
    {
        private bool IsValid(SerializedProperty property) 
        {
            var attr = (ProjectTagAttribute)attribute;
            var tags = ProjectTags.GetTagsByNameSpace(attr.TagNamespace);
            return property.propertyType == SerializedPropertyType.String && tags != null;
        }
        
        protected override float GetPropertyHeight_Internal(SerializedProperty property, GUIContent label)
        {
            return IsValid(property)
                ? GetPropertyHeight(property)
                : GetPropertyHeight(property) + GetHelpBoxHeight();
        }

        protected override void OnGUI_Internal(Rect rect, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(rect, label, property);

            if (property.propertyType != SerializedPropertyType.String)
            {
                DrawDefaultPropertyAndHelpBox(rect, property, $"{nameof(TagAttribute)} supports only string fields", MessageType.Warning);
                EditorGUI.EndProperty(); 
                return;
            }

            var tagList = new List<string>();
            var attr = (ProjectTagAttribute)attribute;
            var tags = ProjectTags.GetTagsByNameSpace(attr.TagNamespace);

            if (tags == null)
            {
                DrawDefaultPropertyAndHelpBox(rect, property, $"ProjectTag Namespace \"{attr.TagNamespace}\" not found!", MessageType.Warning);
                EditorGUI.EndProperty(); 
                return;
            }
            
            tagList.AddRange(tags);

            var propertyString = property.stringValue;
            var index = 0;
            for (int i = 0; i < tagList.Count; i++)
            {
                if (tagList[i].Equals(propertyString, System.StringComparison.Ordinal))
                {
                    index = i;
                    break;
                }
            }
            
            var newIndex = EditorGUI.Popup(rect, label.text, index, tagList.ToArray());
            var newValue = tagList[newIndex];
            if (!property.stringValue.Equals(newValue, System.StringComparison.Ordinal))
                property.stringValue = newValue;

            EditorGUI.EndProperty();
        }
        
        
        
    }
}