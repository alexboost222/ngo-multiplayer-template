using UnityEngine;

namespace Game.Interactables
{
    public interface IInteractable
    {
        GameObject gameObject { get; }
        
        void SetHovered(bool state);

        void Interact();

    }
}