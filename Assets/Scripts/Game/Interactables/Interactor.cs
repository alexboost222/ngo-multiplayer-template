using System.Linq;
using R3;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityExtensions.R3Extensions;

namespace Game.Interactables
{
    public class Interactor : NetworkBehaviour
    {
        [SerializeField] private InputActionReference interact;
        
        private readonly ReactiveProperty<IInteractable> _hovered = new();
        
        public override void OnNetworkSpawn()
        {
            if (!IsOwner) return;
            
            gameObject
                .OnCollidersInTrigger(out var disposableBag)
                .SelectWhereNotNull(r => r.GetComponent<IInteractable>())
                .Subscribe(i => _hovered.Value = i.Any() ? i.OrderBy(i => Vector3.Distance(i.gameObject.transform.position, transform.position)).FirstOrDefault() : null)
                .AddTo(this);

            _hovered
                .AsObservable()
                .Pairwise()
                .Subscribe(tuple =>
                {
                    tuple.Previous?.SetHovered(false);
                    tuple.Current?.SetHovered(true);
                })
                .AddTo(this);
            
            interact.action
                .AsPerformedObservable()
                .SelectWhereNotNull(_ => _hovered.Value)
                .Subscribe(i => i.Interact())
                .AddTo(this);
                
            disposableBag.AddTo(this);
        }
        
        
        
        
    }
}