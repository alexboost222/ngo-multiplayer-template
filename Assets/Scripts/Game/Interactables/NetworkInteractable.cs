using Game.NetworkActions;
using R3;
using TNRD;
using Unity.Netcode;
using UnityEngine;

namespace Game.Interactables
{
    public class NetworkInteractable : NetworkBehaviour, IInteractable
    {
        [SerializeField] private SerializableInterface<INetworkEventHandler<bool>> hoverHandler;
        
        [SerializeField] private SerializableInterface<INetworkEventHandler<Unit>> interactionHandler;
        
        
        public void SetHovered(bool state)
        {
            SendHoveredEventRpc(state);
        }

        public void Interact()
        {
            SendInteractedEventRpc();
        }
        
        
        
        [Rpc(SendTo.Everyone)]
        private void SendInteractedEventRpc()
        {
            interactionHandler.Value.HandleEvent(Unit.Default);
        }
        
        [Rpc(SendTo.Everyone)]
        private void SendHoveredEventRpc(bool eventData)
        {
            hoverHandler.Value.HandleEvent(eventData);
        }
    }
}