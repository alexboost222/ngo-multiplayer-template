using Core.Application;
using R3;
using UnityEngine;
using UnityEngine.UIElements;
using UnityExtensions;
using VContainer;

namespace Game.UI
{
    [RequireComponent(typeof(UIDocument))]
    public class GameUIBinding : MonoBehaviour
    {
        private UIDocument _document;
        private ApplicationFSM _applicationFSM;

        [Inject]
        public void Construct(ApplicationFSM applicationFSM) // TODO Speed crutch
        {
            _applicationFSM = applicationFSM;
            _document = GetComponent<UIDocument>();
            BindGUI();
        }

        private void BindGUI()
        {
            _document
                .rootVisualElement
                .Q<Button>("exit-btn")
                .OnClickAsObservable()
                .Subscribe(_ => _applicationFSM.FSM.PushCommand(ApplicationFSM.Command.BackToMainMenu))
                .AddTo(this);
        }
    }
}