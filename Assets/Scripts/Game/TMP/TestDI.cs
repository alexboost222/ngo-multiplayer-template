using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Game
{
    public class TestDi : NetworkBehaviour
    {
        public void Awake()
        {
            Log("Awake");
        }

        public void Start()
        {
            Log("Start");
        }

        [Inject]
        public void Construct()
        {
            Log("Construct");
        }


        private void Log(string stage)
        {
            if (NetworkManager == null)
            {
                Debug.Log($"{stage}\n" + "NOT INITIALIZED");
            }
            else
            {
                Debug.Log($"{stage}\n" + 
                          $"Client: {NetworkManager.IsClient}\n" +
                          $"Host: {NetworkManager.IsHost}\n" +
                          $"Server: {NetworkManager.IsServer}");
            }
        }


        public override void OnNetworkSpawn()
        {
            Log("NetSpawn");
        }
    }
}