using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace Game.NetworkActions
{
    [SelectionBase]
    public class LightBulb : MonoBehaviour, INetworkEventHandler<bool>
    {
        [SerializeField] private bool defaultState; 
        
        [SerializeField] private new Light light;
        [SerializeField] private new Renderer renderer;
        private bool Active
        {
            get => light.enabled;
            set
            {
                light.enabled = value;
                if (renderer != null) renderer.material.SetKeyword(new LocalKeyword(renderer.material.shader, "_EMISSION"), value);
            }
        }

        private void Awake() => Active = defaultState;
        public void HandleEvent(bool value) => Active = value;
    }
}