using R3;
using UnityEngine;

namespace Game.NetworkActions
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleSystemNetworkEventHandler : MonoBehaviour, INetworkEventHandler<Unit>
    {
        public void HandleEvent(Unit value)
        {
            GetComponent<ParticleSystem>().Play();
        }
    }
}