namespace Game.NetworkActions
{
    public interface INetworkEventHandler<in T>
    {
        void HandleEvent(T value);
    }
}