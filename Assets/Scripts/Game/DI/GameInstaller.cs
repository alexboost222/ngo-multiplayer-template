using Core.Interfaces;
using Core.Networking;
using Game.Player;
using VContainer;
using VContainer.Unity;

namespace Game.DI
{
    public class GameInstaller : ISerializableInstaller
    {
        [Inject]
        public GameInstaller() { }
        
        
        public void Install(IContainerBuilder builder)
        {
            builder.Register<PlayerDataService>(Lifetime.Singleton);
            builder.RegisterEntryPoint<NetworkStarter>();
        }
    }
}