using System;
using Game.Respawn;
using R3;
using Unity.Netcode;
using UnityEngine;
using UnityExtensions.R3Extensions;

namespace Game.Session
{
    [RequireComponent(typeof(Spawner))]
    public class DisableSpawnerInState : NetworkBehaviour
    {
        public GameFSM.State state;
        
        public GameFSM gameFSM;

        public override void OnNetworkSpawn()
        {
            if (!IsServer) return;
            
            gameFSM.currentState
                .AsObservable()
                .Subscribe(tuple =>
                {
                    GetComponent<Spawner>().active.Value = tuple.newValue != state;
                })
                .AddTo(this);
        }
    }
}