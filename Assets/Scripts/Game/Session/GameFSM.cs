using System;
using System.Collections;
using System.Linq;
using Core.Networking;
using Core.Networking.Respawn;
using Cysharp.Threading.Tasks;
using Game.NetworkActions;
using Game.Player;
using R3;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityExtensions.R3Extensions;
using VContainer;

namespace Game.Session
{
    public class GameFSM : NetworkBehaviour, INetworkEventHandler<int>
    {
        public enum State
        {
            WaitingRoom,
            Gameplay,
        }
        
        public readonly ReactiveProperty<float> WaitStartGameTimer = new();
        public float timeToStartSeconds = 3;
        
        
        public readonly ReactiveProperty<float> InGameTimer = new();
        public float inGameTimeSeconds = 60;
        
        public NetworkVariable<State> currentState = new();
        
        [Inject] private PlayerDataService _playerDataService;
        
        private Coroutine _handle;

        [SerializeField] private TMP_Text timerText;
        
        
        public override void OnNetworkSpawn()
        {
            WaitStartGameTimer.AsObservable()
                .Select(v => $"Waiting: {v:F1}")
                .Subscribe(timerText)
                .AddTo(this);
            
            InGameTimer.AsObservable()
                .Select(v => $"Game: {v:F1}")
                .Subscribe(timerText)
                .AddTo(this);


            timerText.text = "";
            
            currentState.AsObservable()
                .Subscribe(tuple =>
                {
                    UniTask.Create(async () =>
                    {
                        await UniTask.Delay(TimeSpan.FromSeconds(0.5f)); 
                        foreach (var respawnable in FindObjectsByType<PlayerAvatar>(FindObjectsSortMode.None)
                                     .Select(p => p.GetComponent<Respawnable>())) 
                            respawnable.TeleportToSpawn();
                    });


                    if (tuple.newValue == State.Gameplay)
                    {
                        StartCoroutine(Timer(InGameTimer, inGameTimeSeconds, () =>
                        {
                            if (IsServer) currentState.Value = State.WaitingRoom;
                        }));
                    }
                })
                .AddTo(this);
        }

        public void HandleEvent(int value)
        {
            if (currentState.Value != State.WaitingRoom) return;
            if (_playerDataService.PlayersData.Count == 0) return;
            
            if (_playerDataService.PlayersData.Count == value)
            {
                if (_handle != null)
                {
                    StopCoroutine(_handle);
                    _handle = null;
                }
                    
                _handle = StartCoroutine(Timer(
                    WaitStartGameTimer, 
                    timeToStartSeconds,
                    () =>
                    {
                        if (IsServer) currentState.Value = State.Gameplay;
                    }));
            }
            else if (_handle != null)
            {
                StopCoroutine(_handle);
                _handle = null;
            }
        }

        private static IEnumerator Timer(ReactiveProperty<float> prop, float seconds, Action callback)
        {
            prop.Value = seconds; 
            while (prop.Value > 0)
            {
                prop.Value -= Time.deltaTime;
                yield return null;
            }
            
            callback.Invoke();
        }
    }
}