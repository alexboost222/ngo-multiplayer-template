using Core.AssetManagement;
using Game.Player;
using Game.Teams;
using R3;
using Unity.Netcode;
using UnityEngine;
using UnityExtensions.R3Extensions;
using VContainer;

namespace Game.Views
{
    public class TeamMaterialColorSetter : NetworkBehaviour
    {
        [SerializeField] private new Renderer renderer;
        private TeamsConfig _teamsConfig;

        [Inject]
        public void Construct(IAssetResolver assetResolver)
        {
            _teamsConfig = assetResolver.LoadSingleAsset<TeamsConfig>();
        }

        public override void OnNetworkSpawn()
        {
            GetComponentInParent<PlayerAvatar>()
                .Self.teamId
                .AsObservable()
                .Subscribe(teamId =>
                {
                    if (_teamsConfig.TryGetData(teamId.newValue, out var data))
                        renderer.material.color = data.Color;
                })
                .AddTo(this);
        }
    }
}