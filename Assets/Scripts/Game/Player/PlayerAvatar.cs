using System;
using System.Linq;
using Core.AssetManagement;
using Core.Networking;
using Game.Teams;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Game.Player
{
    public class PlayerAvatar : NetworkBehaviour
    {
        [field: SerializeField] public PlayerData Self { get; private set; }
        
        [Inject]
        public PlayerDataService PlayerDataService;
        
        [Inject]
        public IAssetResolver AssetResolver;

        private TeamsConfig _teamsConfigs;
        
        public override void OnNetworkSpawn()
        {
            Self = PlayerDataService
                .PlayersData
                .FirstOrDefault(p => p.NetworkObject.OwnerClientId == OwnerClientId) 
                as PlayerData;
            
            _teamsConfigs = AssetResolver.LoadSingleAsset<TeamsConfig>();
        }


#if UNITY_EDITOR
        
        private void OnDrawGizmos()
        {
            if (Self == null) return;
            
            if (_teamsConfigs == null ||
                Self.teamId == null ||
                !_teamsConfigs.TryGetData(Self.teamId.Value, out var teamData)) return;
            
            UnityEditor.Handles.Label(transform.position, teamData.TeamTag);
        }
        
#endif
        
        
    }
}