using R3;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityExtensions.R3Extensions;

namespace Game.Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class AvatarController : NetworkBehaviour
    {
        [SerializeField] private float playerSpeed = 5;

        [SerializeField] public InputActionReference moveAction;
        [SerializeField] public InputActionReference jumpAction;
        
        private Rigidbody _networkRigidbody;
        
        private void Awake()
        {
            _networkRigidbody = GetComponent<Rigidbody>();
        }
        
        public override void OnNetworkSpawn()
        {
            if (!IsOwner) return;

            moveAction.action
                .AsOnUpdateObservable<Vector2>()
                .Where(_ => IsSpawned) //TODO Create AddToThis extension
                .Select(ax => ax * playerSpeed)
                .Select(ax => new Vector3(ax.x, _networkRigidbody.velocity.y, ax.y))
                .Subscribe(ax => _networkRigidbody.velocity = ax)
                .AddTo(this);
            
            moveAction.action
                .AsOnUpdateObservable<Vector2>()
                .Where(_ => IsSpawned) //TODO Create AddToThis extension
                .Select(ax => ax * playerSpeed)
                .Select(ax => new Vector3(ax.x, 0, ax.y))
                .Where(ax => ax.sqrMagnitude > 0.01f)
                .Subscribe(ax => _networkRigidbody.rotation = Quaternion.LookRotation(ax, Vector3.up))
                .AddTo(this);

            jumpAction.action
                .AsPerformedObservable()
                .Where(_ => IsSpawned) //TODO Create AddToThis extension
                .Subscribe(_ => _networkRigidbody.AddForce(_networkRigidbody.rotation * Vector3.forward * 100, ForceMode.Impulse))
                .AddTo(this);
        }
    }
}