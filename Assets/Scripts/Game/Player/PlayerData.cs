using System;
using Core.Networking;
using Unity.Netcode;
using VContainer;

namespace Game.Player
{
    public class PlayerData : NetworkBehaviour, IPlayerData
    {
        NetworkObject IPlayerData.NetworkObject => GetComponent<NetworkObject>();

        public NetworkVariable<ushort> teamId;
        
        [Inject]
        public PlayerDataService PlayerDataService;
        
        
        public override void OnNetworkSpawn()
        {
            PlayerDataService.AddPlayer(this);
        }

        public override void OnNetworkDespawn()
        {
            PlayerDataService.RemovePlayer(this);
        }
    }

}