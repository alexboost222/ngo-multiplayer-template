using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Game.Player
{
    public class PlayerLifetimeScope : LifetimeScope
    {
        [SerializeField] private PlayerAvatar playerAvatar;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(playerAvatar);
        }
    }
}