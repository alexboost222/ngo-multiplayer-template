using System;
using System.Linq;
using UnityEngine;
using UnityExtensions;
using UnityExtensions.TagSystem;

namespace Game.Teams
{
    public class TeamsConfig : ConfigSingleton<TeamsConfig>
    {
        [Serializable]
        public class TeamData
        {
            [field: ProjectTag("Team")]
            [field: SerializeField] public string TeamTag { get; private set; }

            [field: SerializeField] public ushort ID { get; private set; }
            
            [field: SerializeField] public Color Color { get; private set; }
        }

        [SerializeField] private TeamData[] teamsData;


        public bool TryGetData(string teamTag, out TeamData teamData)
        {
            teamData = teamsData.FirstOrDefault(td => td.TeamTag == teamTag);
            return teamData != null;
        }
        
        
        public bool TryGetData(ushort id, out TeamData teamData)
        {
            teamData = teamsData.FirstOrDefault(td => td.ID == id);
            return teamData != null;
        }
        
    }
}