using System.Linq;
using Core.AssetManagement;
using Core.Networking;
using Game.NetworkActions;
using Game.Player;
using UnityEngine;
using UnityExtensions.TagSystem;
using VContainer;

namespace Game.Teams
{
    public class TeamSelector : MonoBehaviour, INetworkEventHandler<IPlayerData>
    {
        [ProjectTag("Team")]
        public string team;
        
        
        private TeamsConfig _teamsConfig;

        [Inject]
        public void Construct(IAssetResolver assetResolver)
        {
            _teamsConfig = assetResolver.LoadSingleAsset<TeamsConfig>();
        }

        public void HandleEvent(IPlayerData value)
        {
            var playerData = value as PlayerData;
            if (playerData == null) return;

            if (_teamsConfig.TryGetData(team, out var teamData))
            {
                playerData.teamId.Value = teamData.ID;
            }
        }
    }
}