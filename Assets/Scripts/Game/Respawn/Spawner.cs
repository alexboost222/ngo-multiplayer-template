using System;
using System.Linq;
using Core.Networking.Respawn;
using Unity.Netcode;
using UnityEngine;

namespace Game.Respawn
{
    public class Spawner : NetworkBehaviour
    {
        [field: SerializeField] public int Priority { get; private set; }
        public NetworkVariable<bool> active = new();
        
        public static Spawner SelectSpawner(Respawnable respawnable)
        {
            var spawner = FindObjectsByType<Spawner>(FindObjectsSortMode.None)
                .Where(s => s.active.Value)
                .OrderByDescending(s => s.Priority)
                .FirstOrDefault();

            if (spawner == null) throw new Exception($"No spawner found for {respawnable}");
            return spawner;
        }
    }
}