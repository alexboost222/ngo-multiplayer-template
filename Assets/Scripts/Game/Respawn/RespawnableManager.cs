using System;
using Cysharp.Threading.Tasks;
using Game.Respawn;
using NaughtyAttributes;
using R3;
using Unity.Netcode;
using UnityEngine;

namespace Core.Networking.Respawn
{
    public class RespawnableManager : NetworkBehaviour
    {
        [field: SerializeField] public Respawnable Respawnable { get; private set; }
        [field: Min(0.5f)]
        [field: SerializeField] public float TimeToRespawnSeconds { get; private set; }
        [field: SerializeField] public bool SameOwnershipAsThis { get; private set; }
        
        private Respawnable _respawnableInstance;
        
        private DisposableBag _disposableBag;
        private UniTask _spawnHandle;

        public override void OnNetworkSpawn()
        {
            if (!IsServer) return;

            Observable
                .EveryUpdate()
                .Where(_ => _respawnableInstance == null)
                .Delay(TimeSpan.FromSeconds(TimeToRespawnSeconds))
                .Where(_ => _respawnableInstance == null)
                .Subscribe(Spawn)
                .AddTo(ref _disposableBag);
        }

        public override void OnNetworkDespawn()
        {
            _disposableBag.Dispose();
        }

        private void Spawn(Unit _ = default)
        {
            var spawnPoint = Spawner.SelectSpawner(Respawnable).transform;
            _respawnableInstance = Instantiate(Respawnable, spawnPoint.position, spawnPoint.rotation);
            
            _respawnableInstance.NetworkObject.SpawnWithOwnership(SameOwnershipAsThis ? OwnerClientId : NetworkManager.ServerClientId);
        }
    }
}