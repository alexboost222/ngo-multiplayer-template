using Game.Respawn;
using Networking;
using Unity.Netcode;
using UnityEngine;

namespace Core.Networking.Respawn
{
    [RequireComponent(typeof(ClientNetworkTransform))]
    public class Respawnable : NetworkBehaviour
    {
        
        [ContextMenu("Despawn")]
        public void Despawn()
        {
            DestroyServerRpc(NetworkObject);
        }
        
        [ContextMenu("TeleportToSpawn")]
        public void TeleportToSpawn()
        {
            TeleportRPC(NetworkObject);
        }

        [Rpc(SendTo.Owner, RequireOwnership = false)]
        private void TeleportRPC(NetworkObjectReference networkObjectReference)
        {
            if (!networkObjectReference.TryGet(out var networkObject) ||
                !networkObject.TryGetComponent<Rigidbody>(out var rb)) return;
            
            var t = Spawner.SelectSpawner(this).transform;
            rb.position = t.position;
            rb.rotation = t.rotation;
        }
        

        [Rpc(SendTo.Server, RequireOwnership = false)]
        private void DestroyServerRpc(NetworkObjectReference networkObjectReference)
        {
            if (networkObjectReference.TryGet(out var networkObject))
            {
                networkObject.Despawn();
            }
            else
            {
                Debug.LogError($"{networkObjectReference} not found!");
            }
        }
        
    }
}