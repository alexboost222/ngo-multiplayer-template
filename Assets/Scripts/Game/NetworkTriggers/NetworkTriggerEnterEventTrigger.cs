using System.Linq;
using Core.Networking;
using Game.NetworkActions;
using Game.Player;
using R3;
using R3.Triggers;
using TNRD;
using Unity.Netcode;
using UnityExtensions.R3Extensions;
using VContainer;

namespace Game.NetworkTriggers
{
    public class NetworkTriggerEnterEventTrigger : NetworkBehaviour
    {
        public SerializableInterface<INetworkEventHandler<IPlayerData>>[] handlers;
        
        [Inject] 
        private PlayerDataService _playerDataService;
        
        public override void OnNetworkSpawn()
        {
            if (!IsServer) return;
            

            gameObject
                .OnTriggerEnterAsObservable()
                .SelectWhereNotNull(c => c.attachedRigidbody)
                .SelectWhereNotNull(r => r.GetComponent<PlayerAvatar>())
                .Subscribe(v => SendEventRpc(v.Self.OwnerClientId))
                .AddTo(this);
        }


        [Rpc(SendTo.Server)]
        public void SendEventRpc(ulong ownerClientId)
        {
            if (!_playerDataService.TryGetById(ownerClientId, out var player)) return;
            foreach (var handler in handlers)
                handler.Value.HandleEvent(player);
        }
    }
}