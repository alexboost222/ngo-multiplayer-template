using Core.Networking;
using Game.Player;
using ObservableCollections;
using R3;
using R3.Triggers;
using UnityExtensions.R3Extensions;

namespace Game.NetworkTriggers
{
    public class CountPlayersInTrigger : NetworkEventTrigger<int>
    {
        private readonly ObservableHashSet<IPlayerData> _players = new();

        private DisposableBag _disposableBag;

        public override void OnNetworkSpawn()
        {
            if (serverOnly && !IsServer) return;
            
            gameObject
                .OnTriggerEnterAsObservable()
                .Where(c => c.CompareTag("Player"))
                .SelectWhereNotNull(c => c.attachedRigidbody)
                .SelectWhereNotNull(r => r.GetComponent<PlayerAvatar>())
                .Subscribe(avatar => _players.Add(avatar.Self))
                .AddTo(ref _disposableBag);
            
            gameObject
                .OnTriggerExitAsObservable()
                .Where(c => c.CompareTag("Player"))
                .SelectWhereNotNull(c => c.attachedRigidbody)
                .SelectWhereNotNull(r => r.GetComponent<PlayerAvatar>())
                .Subscribe(avatar => _players.Remove(avatar.Self))
                .AddTo(ref _disposableBag);
            
            Observable.EveryUpdate()
                .Subscribe(_ => _players.Remove(null))
                .AddTo(ref _disposableBag);


            _players
                .ObserveCountChanged(true)
                .Subscribe(SendEvent)
                .AddTo(ref _disposableBag);
        }
        
        public override void OnNetworkDespawn()
        {
            _disposableBag.Dispose();
        }
    }
}