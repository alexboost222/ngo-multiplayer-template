using System;
using Game.NetworkActions;
using TNRD;
using Unity.Netcode;
using UnityEngine;

namespace Game.NetworkTriggers
{
    [SelectionBase]
    public class AnticipatedNetworkEventTrigger<T> : NetworkBehaviour
    {
        public SerializableInterface<INetworkEventHandler<T>>[] handlers;
        
        private readonly AnticipatedNetworkVariable<T> _event = new(staleDataHandling: StaleDataHandling.Reanticipate);

        private void Awake()
        {
            _event.OnAuthoritativeValueChanged += (
                AnticipatedNetworkVariable<T> _,
                in T value,
                in T newValue) =>
            {
                if (value.Equals(newValue)) return;
                foreach (var handler in handlers) handler.Value.HandleEvent(newValue);
            };
        }


        protected void SetState(T state)
        {
            _event.Anticipate(state);
            foreach (var handler in handlers) handler.Value.HandleEvent(state);
        }
        
        

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (handlers == null) return;
            
            foreach (var handler in handlers)
            {
                if (handler is { Value: not null } && handler.TryGetObject(out var obj))
                {
                    if (obj is Component go)
                    {
                        UnityEditor.Handles.DrawBezier(
                            transform.position,
                            go.transform.position,
                            transform.position + Vector3.up * 2, 
                            go.transform.position + Vector3.up * 2, 
                            (UnityEditor.Selection.activeGameObject == gameObject ||
                             UnityEditor.Selection.activeGameObject == go.gameObject) 
                                ? Color.green : Color.white,
                            null, 
                            10);
                    }
                }
            }
        }
#endif
        
    }
}