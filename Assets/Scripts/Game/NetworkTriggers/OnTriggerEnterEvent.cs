using System.Linq;
using Game.NetworkActions;
using ObservableCollections;
using R3;
using R3.Triggers;
using TNRD;
using Unity.Netcode;
using UnityEngine;


namespace Game.NetworkTriggers
{
    public class OnTriggerEnterEvent : AnticipatedNetworkEventTrigger<bool>
    {
        [SerializeField] private bool serverOnly;
        
        private readonly ObservableHashSet<Collider> _colliders = new(Enumerable.Empty<Collider>());

        private DisposableBag _disposableBag;

        public override void OnNetworkSpawn()
        {
            if (serverOnly && !IsServer) return;
            
            gameObject
                .OnTriggerEnterAsObservable()
                .Where(c => c.CompareTag("Player"))
                .Subscribe(col => _colliders.Add(col))
                .AddTo(ref _disposableBag);
            
            gameObject
                .OnTriggerExitAsObservable()
                .Subscribe(col => _colliders.Remove(col))
                .AddTo(ref _disposableBag);
            
            Observable.EveryUpdate()
                .Subscribe(_ => _colliders.Remove(null))
                .AddTo(ref _disposableBag);
            
            
            _colliders
                .ObserveCountChanged(true)
                .Pairwise()
                .Where(c => c is { Previous: 0, Current: > 0 })
                .Subscribe(_ => SetState(true))
                .AddTo(ref _disposableBag);

            
            _colliders
                .ObserveCountChanged(true)
                .Pairwise()
                .Where(c => c is { Previous: > 0, Current: 0 })
                .Subscribe(_ => SetState(false))
                .AddTo(ref _disposableBag);
        }
        
        public override void OnNetworkDespawn()
        {
            _disposableBag.Dispose();
        }
        
    }
}