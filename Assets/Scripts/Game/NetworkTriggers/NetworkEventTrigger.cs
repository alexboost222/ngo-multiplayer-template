using Game.NetworkActions;
using TNRD;
using Unity.Netcode;

namespace Game.NetworkTriggers
{
    public class NetworkEventTrigger<T> : NetworkBehaviour
    {
        public SerializableInterface<INetworkEventHandler<T>>[] handlers;

        public bool serverOnly;
        
        protected void SendEvent(T eventData)
        {
            if (serverOnly) SendEventServerRpc(eventData);
            else SendEventEveryoneRpc(eventData);
        }
        
        
        [Rpc(SendTo.Everyone)]
        private void SendEventEveryoneRpc(T eventData)
        {
            foreach (var handler in handlers)
                handler.Value.HandleEvent(eventData);
        }
        
        
        [Rpc(SendTo.Server)]
        private void SendEventServerRpc(T eventData)
        {
            foreach (var handler in handlers)
                handler.Value.HandleEvent(eventData);
        }
    }
}