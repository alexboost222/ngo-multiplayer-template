using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VContainer;

namespace Core.AssetManagement
{
    public class ResourcesFolderAssetResolver : IAssetResolver
    {
        [Inject]
        public ResourcesFolderAssetResolver() { }

        public T LoadSingleAsset<T>() where T : Object
        {
            var collection = Resources.LoadAll<T>("").ToList();
            if (collection.Count > 1) Debug.LogError($"More then one {typeof(T)} found!");
            return collection.FirstOrDefault();
        }

        public List<T> LoadAssets<T>() where T : Object
        {
            return Resources.LoadAll<T>("").ToList();
        }

        public List<T> LoadAssetsByComponent<T>() where T : Component
        {
            return LoadAssets<GameObject>()
                .Select(go => go.GetComponent<T>())
                .Where(c => c != null)
                .ToList();
        }

        public List<T> LoadAssetsByInterface<T>()
        {
            return LoadAssets<GameObject>()
                .Select(go => go.GetComponent<T>())
                .Where(c => c != null)
                .ToList();
        }
    }

    public interface IAssetResolver
    {
        public T LoadSingleAsset<T>() where T : Object;
        public List<T> LoadAssets<T>() where T : Object;
        
        public List<T> LoadAssetsByComponent<T>() where T : Component;
        
        public List<T> LoadAssetsByInterface<T>();
    }
}