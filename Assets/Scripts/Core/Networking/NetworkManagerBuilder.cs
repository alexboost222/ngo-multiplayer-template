using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Core.Application;
using Core.AssetManagement;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Object = UnityEngine.Object;

namespace Core.Networking
{
    public class NetworkManagerBuilder
    {
        private readonly IAssetResolver _assetResolver;
        
        private string _ip;
        private ushort _port;
        private RelayServerData? _relayServerData;
        private IEnumerable<NetworkObject> _networkObjectsCollection;

        private NetworkManagerBuilder(IAssetResolver assetResolver)
        {
            _assetResolver = assetResolver;
            
            _ip = "127.0.0.1";
            _port = 7777;
            _relayServerData = null;
            _networkObjectsCollection = Enumerable.Empty<NetworkObject>();
        }


        public static NetworkManagerBuilder CreateInstance(IAssetResolver assetResolver)
        {
            return new NetworkManagerBuilder(assetResolver);
        }

        
        public NetworkManagerBuilder WithLocalhostAddress()
        {
            _ip = "127.0.0.1";
            return this;
        } 
        
        public NetworkManagerBuilder WithLocalNetworkAddress()
        {
            _ip = GetLocalIPAddress();
            return this;
        }
        
        
        public NetworkManagerBuilder WithIP(string ip)
        {
            _ip = ip;
            return this;
        }
        
        
        public NetworkManagerBuilder WithPort(ushort port)
        {
            _port = port;
            return this;
        }
        
        
        public NetworkManagerBuilder WithRelay(RelayServerData relayServerData)
        {
            _relayServerData = relayServerData;
            return this;
        }
        
        public NetworkManagerBuilder WithNetworkObjects(IEnumerable<NetworkObject> collection)
        {
            _networkObjectsCollection = collection;
            return this;
        }
        

        public NetworkManager Build()
        {
            var transportType = _relayServerData.HasValue ? 
                UnityTransport.ProtocolType.RelayUnityTransport : 
                UnityTransport.ProtocolType.UnityTransport;
            
            var prefab = _assetResolver
                .LoadAssetsByComponent<NetworkManager>()
                .First(nm => nm.GetComponent<UnityTransport>().Protocol == transportType);

            var networkManager = Object.Instantiate(prefab);

            var transport = networkManager.GetComponent<UnityTransport>();
            transport.ConnectionData = ConnectionData();
            if (_relayServerData.HasValue) transport.SetRelayServerData(_relayServerData.Value);

#pragma warning disable CS0162 // Unreachable code detected
            if (NetworkStarter.ConnectionType == "wss")
                transport.UseWebSockets = true;
#pragma warning restore CS0162 // Unreachable code detected

            foreach (var no in _networkObjectsCollection)
                networkManager.AddNetworkPrefab(no.gameObject);
            
            return networkManager;
        }

        private UnityTransport.ConnectionAddressData ConnectionData()
        {
            return new UnityTransport.ConnectionAddressData
            {
                Address = _ip,
                ServerListenAddress = _ip,
                Port = _port,
            };
        }
        
        private static string GetLocalIPAddress()
        {
            return Dns
                .GetHostEntry(Dns.GetHostName())
                .AddressList
                .First(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .ToString();
        }
    }
}