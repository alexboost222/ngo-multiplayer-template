using System.Linq;
using ObservableCollections;
using R3;
using UnityEngine;
using VContainer;

namespace Core.Networking
{
    public class PlayerDataService
    {
        public ObservableList<IPlayerData> PlayersData { get; }

        [Inject]
        public PlayerDataService()
        {
            PlayersData = new ObservableList<IPlayerData>();
        }


        public bool TryGetById(ulong id, out IPlayerData pd)
        {
            pd = PlayersData.FirstOrDefault(p => p.ClientID == id);
            return pd != null;
        }


        public void AddPlayer(IPlayerData playerData)
        {
            PlayersData.Add(playerData);
        }
        
        public void RemovePlayer(IPlayerData playerData)
        {
            PlayersData.Remove(playerData);
        }
    }
}