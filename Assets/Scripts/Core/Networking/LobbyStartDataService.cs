using System;
using Core.Application;
using Unity.Networking.Transport.Relay;
using VContainer;

namespace Core.Networking
{
    public class LobbyStartDataService
    {
        [Inject]
        public LobbyStartDataService() { }
        
        public bool Initialized { get; private set; }
        
        private RelayServerData? _relayData;
        private int? _playersInLobby;
        private NetworkRole? _role;


        public NetworkRole Role
        {
            get
            {
                if (!_role.HasValue)
                    throw new Exception("Lobby _role is empty");
                
                return _role.Value;
            }
            set
            {
                Initialized = true;
                _role = value;
            }
        }
        
        public int PlayersInLobby
        {
            get
            {
                if (!_playersInLobby.HasValue)
                    throw new Exception("Lobby _playersInLobby is empty");
                
                return _playersInLobby.Value;
            }
            set
            {
                Initialized = true;
                _playersInLobby = value;
            }
        }

        public RelayServerData RelayServerData
        {
            get
            {
                if (!_relayData.HasValue)
                    throw new Exception("Lobby _relayData is empty");
                
                return _relayData.Value;
            }
            set
            {
                Initialized = true;
                _relayData = value;
            }
        }
    }
}