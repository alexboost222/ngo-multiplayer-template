using Unity.Netcode;

namespace Core.Networking
{
    public interface IPlayerData
    {
        NetworkObject NetworkObject { get; }

        public ulong ClientID => NetworkObject.OwnerClientId;
    }
}