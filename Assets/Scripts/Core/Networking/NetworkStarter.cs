using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core.Application;
using Core.AssetManagement;
using Cysharp.Threading.Tasks;
using Unity.Netcode;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using Object = UnityEngine.Object;

namespace Core.Networking
{
    public class NetworkStarter : IAsyncStartable, IDisposable
    {
        
#if UNITY_WEBGL
        public const string ConnectionType = "wss"; 
#else
        public const string ConnectionType = "udp";
#endif
        
        [Inject] private PlayerDataService _playerDataService;
        [Inject] private IAssetResolver _assetResolver;
        [Inject] private EnterPlayModeOptions _enterPlayModeOptions;
        [Inject] private LobbyStartDataService _lobbyStartData;
        
        private readonly TimeSpan _requestDelay = TimeSpan.FromSeconds(2.1);
        private const string LogTag = "[MONO_LOBBY_SETUP]";
        private const string DebugMonoLobbyName = "DEBUG_MONO_LOBBY";
        
        private NetworkRole _role;
        private NetworkSetup _networkSetup;
        private NetworkManager _networkManager;


        public async UniTask StartAsync(CancellationToken cancellation)
        {
            _role = _enterPlayModeOptions.NetworkRole;
            _networkSetup = _enterPlayModeOptions.NetworkSetup;

            // TODO Crutch. We need to decide this earlier at DI stage
            if (_lobbyStartData.Initialized)
            {
                _networkSetup = NetworkSetup.Lobby;
                _role = _lobbyStartData.Role;
                
                Debug.Log("LobbyStartData:\n" + 
                          $"RelayServerData.AllocationId: {_lobbyStartData.RelayServerData.Endpoint.Address}\n" +
                          $"Role: {_lobbyStartData.Role}\n" + 
                          $"PlayersInLobby: {_lobbyStartData.PlayersInLobby}\n");
            } 
             
            
            var builder = NetworkManagerBuilder
                .CreateInstance(_assetResolver)
                .WithNetworkObjects(_assetResolver.LoadAssetsByComponent<NetworkObject>());
            
            
            builder = _networkSetup switch
            {
                NetworkSetup.Local => builder.WithLocalhostAddress(),
                NetworkSetup.LocalNetworkExposed => builder.WithLocalNetworkAddress(),
                NetworkSetup.MonoLobby => builder.WithRelay(await GetMonoLobbyRelayData()),
                NetworkSetup.Lobby => builder.WithRelay(_lobbyStartData.RelayServerData),
                _ => throw new NotImplementedException(),
            };
            
            var networkManager = builder.Build();
            
            if (_role == NetworkRole.Host)
                networkManager.OnClientConnectedCallback += clientId =>
                {
                    var playerData = _assetResolver.LoadAssetsByInterface<IPlayerData>().First();
                    networkManager.SpawnManager.InstantiateAndSpawn(playerData.NetworkObject, clientId, isPlayerObject: true);
                };
            
            switch (_role)
            {
                case NetworkRole.Host:
                    networkManager.StartHost();
                    break;
                case NetworkRole.Client:
                    networkManager.StartClient();
                    break;
                case NetworkRole.Unknown:
                default:
                    throw new ArgumentOutOfRangeException(nameof(_role), _role, null);
            }
            
            await UniTask.WaitUntil(() => networkManager.IsListening, cancellationToken: cancellation);
            _networkManager = networkManager;
        }
        
        

        public void Dispose()
        {
            if (_networkManager == null) return;
            
            _networkManager.Shutdown();
            Object.Destroy(_networkManager.gameObject);
        }


        private async UniTask<RelayServerData> GetMonoLobbyRelayData()
        {
            await UniTask.WaitUntil(() => AuthenticationService.Instance.IsAuthorized);
            
            if (_role == NetworkRole.Host)
            {
                await DestroyJoinedLobbies();

                var lobby = await LobbyService.Instance.CreateLobbyAsync(DebugMonoLobbyName, 4, new CreateLobbyOptions
                {
                    IsPrivate = false
                });
                Debug.Log($"{LogTag} Created lobby {lobby.Id}");
                await UniTask.Delay(_requestDelay);
            
                var allocation = await RelayService.Instance.CreateAllocationAsync(4);
                Debug.Log($"{LogTag} Created allocation");
                await UniTask.Delay(_requestDelay);
                
                var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
                Debug.Log($"{LogTag} Got join code");
                await UniTask.Delay(_requestDelay);
            
                await LobbyService.Instance.UpdateLobbyAsync(lobby.Id, new UpdateLobbyOptions
                {
                    Data = new Dictionary<string, DataObject>
                    {
                        ["RelayJoinCode"] = new(DataObject.VisibilityOptions.Public, joinCode)
                    }
                });
                Debug.Log($"{LogTag} Updated lobby");
            
                Debug.Log($"{LogTag} Join code: {joinCode}");

                return new RelayServerData(allocation, ConnectionType);
            }

            if (_role == NetworkRole.Client)
            {
                Lobby lobby = null;
                while (lobby == null)
                {
                    var lobbies = await Lobbies.Instance.QueryLobbiesAsync(new QueryLobbiesOptions
                    {
                        Filters = new List<QueryFilter>
                        {
                            new(QueryFilter.FieldOptions.Name, DebugMonoLobbyName, QueryFilter.OpOptions.EQ),
                        }
                    });
                    await UniTask.Delay(_requestDelay);

                    if (lobbies.Results.Count != 1)
                    {
                        Debug.LogWarning($"{LogTag} Multiple mono lobbies found!");
                        Debug.Log($"{LogTag} Waiting while only one mono lobby will exists!");
                        continue;
                    }
                    
                    if (lobbies.Results.Count > 0 && lobbies.Results.First().Players.Count > 0)
                    {
                        await UniTask.Delay(_requestDelay);
                        lobby = await Lobbies.Instance.JoinLobbyByIdAsync(lobbies.Results.First().Id);
                    }
                
                    Debug.Log($"{LogTag} Lobby not found");
                    await UniTask.Delay(_requestDelay);
                }
            
                Debug.Log($"{LogTag} LobbyId: {lobby.Id}");
                var joinCode = "";
                while (string.IsNullOrEmpty(joinCode))
                {
                    lobby = await LobbyService.Instance.GetLobbyAsync(lobby.Id);
                    joinCode = lobby.Data?["RelayJoinCode"].Value;

                    if (string.IsNullOrEmpty(joinCode))
                        Debug.Log($"{LogTag} No join code");
                    await UniTask.Delay(_requestDelay);
                }
                
                Debug.Log($"{LogTag} {joinCode}");
            
                var allocation = await RelayService.Instance.JoinAllocationAsync(joinCode: joinCode);
                return new RelayServerData(allocation, ConnectionType);
            }
            
            throw new ArgumentException($"Can't start with role {NetworkRole.Unknown}");
        }

        private async UniTask DestroyJoinedLobbies()
        {
            var lobbyIds = await LobbyService.Instance.GetJoinedLobbiesAsync();

            if (lobbyIds.Count == 0)
            {
                return;    
            }
            
            Debug.Log($"{LogTag} LobbyIds: {string.Join(", ", lobbyIds)}");
            await UniTask.Delay(_requestDelay);
            
            foreach (var id in lobbyIds)
            {
                var lobby = await LobbyService.Instance.GetLobbyAsync(id);
                
                var isHost = lobby.HostId == AuthenticationService.Instance.PlayerId;
                if (isHost)
                    await LobbyService.Instance.DeleteLobbyAsync(id);
                else
                    await LobbyService.Instance.RemovePlayerAsync(lobby.Id,
                        AuthenticationService.Instance.PlayerId);

                Debug.Log($"{LogTag} {(isHost ? "Deleted" : "Left")} lobby {id}");
                await UniTask.Delay(_requestDelay);
            }
        }
    }
}