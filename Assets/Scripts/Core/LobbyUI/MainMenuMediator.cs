using System;
using System.Linq;
using UnityEngine;
using VContainer;

namespace Core.UI
{
    public class MainMenuMediator : MonoBehaviour
    {

        [Serializable]
        public struct Pair
        {
            public MainMenuFSM.State state;
            public UIDocumentBindingBase binding;
        }

        public Pair[] pairs;
        
        
        [Inject]
        public void Construct(MainMenuFSM fsm)
        {
            foreach (var pair in pairs)
            {
                fsm.FSM
                    .OnEnter(pair.state, () => pair.binding.Active = true)
                    .OnExit(pair.state, () => pair.binding.Active = false);

                pair.binding.Active = fsm.FSM.CurrentState.CurrentValue == pair.state;
            }
        }
    }
}