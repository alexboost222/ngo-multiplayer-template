using System;
using Core.Application;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace Core.UI
{
    [RequireComponent(typeof(UIDocument))]
    public abstract class UIDocumentBindingBase : MonoBehaviour
    {
        protected UIDocument Document { get; private set; }
        protected LobbyPresenter LobbyPresenter { get; private set; }

        public bool Active
        {
            get => Document.rootVisualElement.visible;
            set => Document.rootVisualElement.visible = value;
        }

        private void Awake()
        {
            Document = GetComponent<UIDocument>();
        }

        [Inject]
        public virtual void Construct(LobbyPresenter lobbyPresenter)
        {
            LobbyPresenter = lobbyPresenter;
            BindGUI();
        }
        protected abstract void BindGUI();
    }
}