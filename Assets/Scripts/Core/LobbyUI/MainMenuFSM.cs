using UnityEngine;
using VContainer;

namespace Core.UI
{
    public class MainMenuFSM
    {
        public enum State
        {
            Main,
            LobbyList,
            Lobby,
        }
        
        public enum Command
        {
            OpenLobbyList,
            BackToMenu,
            OpenLobby,
            ExitLobby,
        }

        public LocalFSM<State, Command> FSM;

        [Inject]
        public MainMenuFSM()
        {
            FSM = LocalFSM<State, Command>.CreateInstance(State.Main, LogType.Log)
                .RegisterTransition(State.Main, State.LobbyList, Command.OpenLobbyList)
                .RegisterTransition(State.LobbyList, State.Main, Command.BackToMenu)
                .RegisterTransition(State.LobbyList, State.Lobby, Command.OpenLobby)
                .RegisterTransition(State.Lobby, State.LobbyList, Command.ExitLobby);
        }
    }
}