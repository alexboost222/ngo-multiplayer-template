using Core.UI;
using R3;
using UnityEngine;
using UnityEngine.UIElements;
using UnityExtensions;

namespace Core.LobbyUI.MainPanel
{
    [RequireComponent(typeof(UIDocument))]
    public class MainMenuBinding : UIDocumentBindingBase
    {
        protected override void BindGUI()
        {
            Document
                .rootVisualElement
                .Q<Button>("play-btn")
                .OnClickAsObservable()
                .Subscribe(LobbyPresenter.OpenLobbyList)
                .AddTo(this);
                

            Document
                .rootVisualElement
                .Q<Button>("settings-btn")
                .OnClickAsObservable()
                .Subscribe(LobbyPresenter.OpenSettings)
                .AddTo(this);
            
            Document
                .rootVisualElement
                .Q<Button>("exit-btn")
                .OnClickAsObservable()
                .Subscribe(LobbyPresenter.ExitApplication)
                .AddTo(this);
        }
    }
}