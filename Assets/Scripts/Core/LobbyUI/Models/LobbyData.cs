using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.Application;
using Cysharp.Threading.Tasks;
using ObservableCollections;
using R3;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;

namespace Core.UI.Models
{
    public class LobbyData
    {
        public readonly ReactiveProperty<string> LobbyId = new();
        public readonly ReactiveProperty<int> MaxPlayers = new();
        public readonly ReactiveProperty<string> Name = new();
        public readonly ReactiveProperty<bool> IsHost = new();
        
        public readonly ReactiveProperty<string> GameStartRelayJoinCode = new();

        
        public readonly ObservableList<PlayerEntry> Players = new();

        public void ApplyLobby(Lobby lobby)
        {
            LobbyId.Value = lobby.Id;
            MaxPlayers.Value = lobby.MaxPlayers;
            Name.Value = lobby.Name;
            IsHost.Value = lobby.HostId == AuthenticationService.Instance.PlayerId;

            if (lobby.Data != null && lobby.Data.TryGetValue("joinCode", out var joinCode))
                GameStartRelayJoinCode.Value = joinCode.Value;

            Players.Clear();
            foreach (var player in lobby.Players)
                Players.Add(PlayerEntry.Create(player));
        }
    }

    public struct PlayerEntry
    {
        public readonly string Id;
        public readonly string Name;

        public static PlayerEntry Create(Player player)
        {
            return new PlayerEntry(player.Id, player.Data["LobbyPlayerName"].Value);
        }

        private PlayerEntry(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}