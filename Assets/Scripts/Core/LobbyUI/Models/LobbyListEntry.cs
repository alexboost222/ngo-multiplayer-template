using Unity.Services.Lobbies.Models;

namespace Core.UI.Models
{
    public struct LobbyListEntry
    {
        public string LobbyName;
        public string LobbyId;
        
        public string Slots; // TODO speed crutch

        public LobbyListEntry(Lobby lobby)
        {
            LobbyName = lobby.Name;
            LobbyId = lobby.Id;

            Slots = $"{lobby.MaxPlayers - lobby.AvailableSlots} / {lobby.MaxPlayers}";
        }
    }
}