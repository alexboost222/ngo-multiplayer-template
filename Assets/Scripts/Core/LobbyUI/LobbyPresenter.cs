using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Application;
using Core.Networking;
using Core.UI.Models;
using Cysharp.Threading.Tasks;
using ObservableCollections;
using R3;
using Unity.Multiplayer.Playmode;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using UnityEngine;
using UnityExtensions;
using VContainer;
using Result = UnityExtensions.Result;

namespace Core.UI
{
    public class LobbyPresenter : IDisposable
    {
        private readonly MainMenuFSM _mainMenuFSM;
        private readonly ApplicationFSM _applicationFSM;
        private readonly LobbyStartDataService _lobbyStartDataService;

        public readonly ObservableList<LobbyListEntry> LobbiesList;
        public readonly LobbyData LobbyData;

        public readonly Observable<bool> StartGameButtonVisible;

        private DisposableBag _disposableBag;
        private RelayServerData _hostRelayData; // TODO Crutch

        [Inject]
        public LobbyPresenter(MainMenuFSM mainMenuFSM, ApplicationFSM applicationFSM, LobbyStartDataService lobbyStartDataService)
        {
            _mainMenuFSM = mainMenuFSM;
            _applicationFSM = applicationFSM;
            _lobbyStartDataService = lobbyStartDataService;
            
            LobbiesList = new ObservableList<LobbyListEntry>();
            LobbyData = new LobbyData();

            StartGameButtonVisible = Observable
                .CombineLatest(
                    LobbyData.IsHost,
                    _mainMenuFSM.FSM.CurrentState.Select(s => s == MainMenuFSM.State.Lobby)
                ).Select(a => a.All());

            LobbyData.GameStartRelayJoinCode
                .Where(v => !string.IsNullOrEmpty(v))
                .Select(_ => Unit.Default)
                .Subscribe(_ =>
                {
                    UniTask.Create(async () =>
                    {
                        var result = await StartGame();
                        if (result.IsError) Debug.LogError(result.Error);
                    });
                })
                .AddTo(ref _disposableBag);
        }
        
        public void OpenLobbyList(Unit _ = default)
        {
            _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.OpenLobbyList);

            RefreshLobbyList().Forget();
        }

        public void OpenSettings(Unit _ = default) => Debug.Log("Settings in progress");
        public void ExitApplication(Unit _ = default) => UnityEngine.Application.Quit();
        public void BackToMain(Unit _ = default) => _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.BackToMenu);
        public async UniTask<Result> StartGameHost() =>
            await Result.Wrap(async () =>
            {
                var allocation = await RelayService.Instance.CreateAllocationAsync(4);
                _hostRelayData = new RelayServerData(allocation, NetworkStarter.ConnectionType);
                
                var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

                LobbyData.ApplyLobby(await LobbyService.Instance.UpdateLobbyAsync(LobbyData.LobbyId.Value,
                    new UpdateLobbyOptions
                    {
                        Data = new Dictionary<string, DataObject>
                        {
                            { "joinCode", new DataObject(DataObject.VisibilityOptions.Public, joinCode) },
                            { "players", new DataObject(DataObject.VisibilityOptions.Public, LobbyData.Players.Count.ToString()) }
                        }
                    }));
            });

        private async UniTask<Result> StartGame() =>
            await Result.Wrap(async () =>
            {
                Debug.Log(LobbyData.GameStartRelayJoinCode.Value);

                if (LobbyData.IsHost.Value)
                {
                    Debug.Log("Host relay data");
                    _lobbyStartDataService.RelayServerData = _hostRelayData;
                }
                else
                {
                    var allocation = await RelayService.Instance.JoinAllocationAsync(LobbyData.GameStartRelayJoinCode.Value);
                    _lobbyStartDataService.RelayServerData = new RelayServerData(allocation, NetworkStarter.ConnectionType);    
                }

                _lobbyStartDataService.PlayersInLobby = LobbyData.Players.Count;
                _lobbyStartDataService.Role = LobbyData.IsHost.Value ? NetworkRole.Host : NetworkRole.Client;

                _applicationFSM.FSM.PushCommand(ApplicationFSM.Command.StartGame);
            });

        public async UniTask<Result> RefreshLobbyList() => await Result.Wrap(async () =>
        {
            var options = new QueryLobbiesOptions
            {
                Count = 25,
                Filters = new List<QueryFilter>
                {
                    new(
                        field: QueryFilter.FieldOptions.AvailableSlots,
                        op: QueryFilter.OpOptions.GT,
                        value: "0")
                },
                Order = new List<QueryOrder>
                {
                    new(
                        asc: false,
                        field: QueryOrder.FieldOptions.Created)
                }
            };

            var lobbies = await Lobbies.Instance.QueryLobbiesAsync(options);

            LobbiesList.Clear();
            foreach (var lobby in lobbies.Results) 
                LobbiesList.Add(new LobbyListEntry(lobby));
        });

        public async UniTask<Result> CreateLobby() => await Result.Wrap(async () =>
        {
            const int maxPlayers = 4;
            var lobbyName = RandomGenerator.GetRandomLobbyName();
            var options = new CreateLobbyOptions
            {
                Player = new Player
                {
                    Data = new Dictionary<string, PlayerDataObject>
                    {
                        { LobbyPlayerNameKey, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, SelfName) }
                    }
                }
            };
            var lobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, maxPlayers, options);

            LobbyData.ApplyLobby(lobby);
            _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.OpenLobby);

            await UniTask.Delay(TimeSpan.FromSeconds(1));
            
            var callbacks = new LobbyEventCallbacks();
            callbacks.PlayerJoined += _ => RefreshLobby().Forget();
            callbacks.PlayerLeft += _ => RefreshLobby().Forget();
            callbacks.DataChanged += _ => RefreshLobby().Forget();
            callbacks.PlayerDataChanged += _ => RefreshLobby().Forget();
            
            await LobbyService.Instance.SubscribeToLobbyEventsAsync(lobby.Id, callbacks);
        });

        public async UniTask<Result> ExitLobby() =>
            await Result.Wrap(async () =>
            {
                if (LobbyData.IsHost.Value)
                    await LobbyService.Instance.DeleteLobbyAsync(LobbyData.LobbyId.Value);
                else
                    await LobbyService.Instance.RemovePlayerAsync(LobbyData.LobbyId.Value,
                        AuthenticationService.Instance.PlayerId);
                
                _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.ExitLobby);
            });

        public async UniTask<Result> JoinLobby(string lobbyId) => await Result.Wrap(async () =>
        {
            var lobby = await LobbyService.Instance.JoinLobbyByIdAsync(lobbyId, new JoinLobbyByIdOptions
            {
                Player = new Player
                {
                    Data = new Dictionary<string, PlayerDataObject>
                    {
                        {LobbyPlayerNameKey, new PlayerDataObject(PlayerDataObject.VisibilityOptions.Public, SelfName)}
                    }
                }
            });
            
            LobbyData.ApplyLobby(lobby);
            
            var callbacks = new LobbyEventCallbacks();
            callbacks.PlayerJoined += _ => RefreshLobby().Forget();
            callbacks.PlayerLeft += _ => RefreshLobby().Forget();
            callbacks.DataChanged += _ => RefreshLobby().Forget();
            callbacks.PlayerDataChanged += _ => RefreshLobby().Forget();
            callbacks.KickedFromLobby += () => _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.ExitLobby);
            
            await LobbyService.Instance.SubscribeToLobbyEventsAsync(lobby.Id, callbacks);
            
            _mainMenuFSM.FSM.PushCommand(MainMenuFSM.Command.OpenLobby);
        });

        public async UniTask<Result> RefreshLobby() => await Result.Wrap(async () =>
        {
            LobbyData.ApplyLobby(await LobbyService.Instance.GetLobbyAsync(LobbyData.LobbyId.Value));
            Debug.Log("Lobby Refreshed");
        });
        
        #region Crutch // TODO Remove

        private const string LobbyPlayerNameKey = "LobbyPlayerName";
        private static string LobbyPlayerNameMultiplayKey => $"LobbyPlayerName_{CurrentPlayer.ReadOnlyTags().FirstOrDefault()}";

        // TODO crutch to avoid creating Settings provider at this stage
        private static string SelfName
        {
            get
            {
                if (!PlayerPrefs.HasKey(LobbyPlayerNameMultiplayKey))
                    PlayerPrefs.SetString(LobbyPlayerNameMultiplayKey, RandomGenerator.GetRandomFullName());

                return PlayerPrefs.GetString(LobbyPlayerNameMultiplayKey);
            }
        }

        #endregion

        public void Dispose()
        {
            _disposableBag.Dispose();
        }
    }
}