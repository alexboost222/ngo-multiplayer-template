using Core.UI;
using R3;
using UnityEngine;
using UnityEngine.UIElements;
using UnityExtensions;

namespace Core.LobbyUI.LobbiesPanel
{
    [RequireComponent(typeof(UIDocument))]
    public class LobbiesBinding : UIDocumentBindingBase
    {
        protected override void BindGUI()
        {
            Document.rootVisualElement
                .Q<Button>("exit-btn")
                .OnClickAsObservable()
                .Subscribe(LobbyPresenter.BackToMain)
                .AddTo(this);

            Document.rootVisualElement.Q<Button>("create-btn")
                .AsyncClicked(LobbyPresenter.CreateLobby)
                .AddTo(this);

            Document.rootVisualElement.Q<Button>("refresh-btn")
                .AsyncClicked(LobbyPresenter.RefreshLobbyList)
                .AddTo(this);

            Document.rootVisualElement.Q<ListView>()
                .BindTo(LobbyPresenter.LobbiesList, (e, item) =>
                {
                    e.Q<Label>("lobby-name").text = item.LobbyName;
                    e.Q<Label>("lobby-id").text = item.LobbyId;
                    e.Q<Label>("lobby-slots").text = item.Slots;
                    e.Q<Button>("join-btn")
                        .AsyncClicked(() => LobbyPresenter.JoinLobby(item.LobbyId))
                        .AddTo(this);
                }).AddTo(this);
        }
    }
}