using Core.UI;
using R3;
using UnityEngine;
using UnityEngine.UIElements;
using UnityExtensions;

namespace Core.LobbyUI.LobbyPanel
{
    [RequireComponent(typeof(UIDocument))]
    public class LobbyBinding : UIDocumentBindingBase
    {
        protected override void BindGUI()
        {
            Document.rootVisualElement
                .Q<Button>("refresh-btn")
                .AsyncClicked(LobbyPresenter.RefreshLobby)
                .AddTo(this);
            
            Document.rootVisualElement.Q<Button>("start-game")
                .VisibleBindTo(LobbyPresenter.StartGameButtonVisible)
                .AddTo(this);
            
            Document.rootVisualElement
                .Q<Button>("exit-btn")
                .AsyncClicked(LobbyPresenter.ExitLobby)
                .AddTo(this);

            Document.rootVisualElement
                .Q<Button>("start-game")
                .AsyncClicked(LobbyPresenter.StartGameHost)
                .AddTo(this);
            
            Document.rootVisualElement
                .Q<Label>("lobby-name")
                .BindTo(LobbyPresenter.LobbyData.Name)
                .AddTo(this);
            
            Document.rootVisualElement
                .Q<Label>("lobby-id")
                .BindTo(LobbyPresenter.LobbyData.LobbyId)
                .AddTo(this);

            LobbyPresenter.LobbyData.Players.BindTo(Document.rootVisualElement.Q<ListView>(),
                (e, item) =>
                {
                    e.Q<Label>("lobby-player-name").text = item.Name;
                    e.Q<Label>("lobby-player-id").text = item.Id;
                }).AddTo(this);
        }
    }
}