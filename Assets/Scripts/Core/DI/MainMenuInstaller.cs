using Core.Interfaces;
using Core.UI;
using VContainer;

namespace Core.DI
{
    public class MainMenuInstaller : ISerializableInstaller
    {
        public void Install(IContainerBuilder builder)
        {
            builder.Register<MainMenuFSM>(Lifetime.Singleton);
            builder.Register<LobbyPresenter>(Lifetime.Singleton);
        }
    }
}