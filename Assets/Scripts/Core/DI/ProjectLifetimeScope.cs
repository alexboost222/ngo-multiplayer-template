using Core.Application;
using Core.AssetManagement;
using Core.LevelManagement;
using Core.Networking;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core.DI
{
    public class ProjectLifetimeScope : LifetimeScope
    {
        [SerializeField] private MainCameraSingleton mainCameraPrefab;
        
        protected override void Configure(IContainerBuilder builder)
        {
            Instantiate(mainCameraPrefab); // TODO It's kinda crutch
            
            builder.Register<ResourcesFolderAssetResolver>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.Register<LevelManager>(Lifetime.Singleton);
            builder.Register<ApplicationFSM>(Lifetime.Singleton);
            builder.Register<EnterPlayModeOptions>(Lifetime.Singleton);
            builder.Register<LobbyStartDataService>(Lifetime.Singleton);
            
            builder.RegisterEntryPoint<LoginService>();
            builder.RegisterEntryPoint<EnterPlayMode>();
        }
    }
}