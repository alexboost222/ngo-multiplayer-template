using System.Collections.Generic;
using System.Linq;
using Core.UnityExtensions;
using Unity.Multiplayer.Playmode;
using UnityEngine;
using VContainer;

namespace Core.Application
{
   
    public enum NetworkSetup
    {
        Local,
        LocalNetworkExposed,
        MonoLobby,
        Lobby,
    }
        
    public enum NetworkRole
    {
        Host,
        Client,
        Unknown,
    }
    
    public class EnterPlayModeOptions
    {
        private readonly Dictionary<string, NetworkSetup> _tagsToStrings = new()
        {
            { "Local", NetworkSetup.Local},
            { "LocalNetworkExposed", NetworkSetup.LocalNetworkExposed},
            { "MonoLobby", NetworkSetup.MonoLobby},
            { "Lobby", NetworkSetup.Lobby},
        };
        
        [Inject]
        public EnterPlayModeOptions()
        {
            EnterFromState = EnumEditorPrefs.GetOrDefault(ApplicationFSM.State.EntryPoint);;
            NetworkSetup = EnumEditorPrefs.GetOrDefault(NetworkSetup.Local);
            NetworkRole = EnumEditorPrefs.GetOrDefault(NetworkRole.Host);

#if UNITY_EDITOR
            var tags = CurrentPlayer.ReadOnlyTags();
            if (tags.Length > 0)
            {
                NetworkRole = tags.Contains("Client") ? NetworkRole.Client : NetworkRole.Host;

                foreach (var pair in _tagsToStrings.Where(pair => tags.Contains(pair.Key)))
                {
                    NetworkSetup = pair.Value;
                }
            }
#endif
            
            Debug.Log("EnterPlayModeOptions:\n" +
                      $"enterFromState: {EnterFromState}\n" + 
                      $"networkSetup: {NetworkSetup}\n" + 
                      $"networkRole: {NetworkRole}\n");
        }
            
        public readonly ApplicationFSM.State EnterFromState;
        public readonly NetworkSetup NetworkSetup;
        public readonly NetworkRole NetworkRole;
    }
}