using System;
using UnityEngine;
using VContainer;

namespace Core.Application
{
    public class EntryPoint : MonoBehaviour
    {
        [Inject]
        public void Construct(ApplicationFSM applicationFSM)
        {
            applicationFSM.FSM.PushCommand(ApplicationFSM.Command.LoadResources);
        }
    }
}