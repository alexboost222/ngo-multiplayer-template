using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Unity.Multiplayer.Playmode;
using Unity.Services.Authentication;
using Unity.Services.Core;
using UnityExtensions;
using VContainer;
using VContainer.Unity;

namespace Core.Application
{
    public class LoginService : IAsyncStartable
    {
        [Inject]
        public LoginService() { }
        
        public async UniTask StartAsync(CancellationToken cancellation)
        {
            var options = new InitializationOptions();
            options.SetProfile(CurrentPlayer.ReadOnlyTags().FirstOrDefault() ?? "Main");
            await UnityServices.InitializeAsync(options);
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
    }
}