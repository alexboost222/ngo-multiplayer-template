using System.Linq;
using Core.AssetManagement;
using Core.LevelManagement;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;

namespace Core.Application
{
    public class ApplicationFSM
    {
        public enum State
        {
            EntryPoint,
            MainMenu,
            Game
        }
        
        public enum Command
        {
            LoadResources,
            StartGame,
            BackToMainMenu
        }
        
        public readonly LocalFSM<State, Command> FSM;


        [Inject]
        public ApplicationFSM(LevelManager levelManager, IAssetResolver assetResolver)
        {
            var state = State.EntryPoint;
#if UNITY_EDITOR
            var activeSceneName = SceneManager.GetActiveScene().name;
            state = assetResolver
                .LoadAssets<Environment>()
                .FirstOrDefault(env => env.SceneName == activeSceneName)!.State;
#endif
            
            FSM = LocalFSM<State, Command>
                .CreateInstance(state, LogType.Log)
                .RegisterTransition(State.EntryPoint, State.MainMenu, Command.LoadResources)
                .RegisterTransition(State.MainMenu, State.Game, Command.StartGame)
                .RegisterTransition(State.Game, State.MainMenu, Command.BackToMainMenu);

            LoadLevelsOnTransitions(levelManager, assetResolver);
        }
        
        private void LoadLevelsOnTransitions(LevelManager levelManager, IAssetResolver assetResolver)
        {
            foreach (var env in assetResolver.LoadAssets<Environment>()) 
                FSM.OnEnter(env.State, () => levelManager.LoadEnv(env).Forget());
        }
    }
}