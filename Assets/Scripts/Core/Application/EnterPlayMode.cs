using System;
using System.Linq;
using System.Threading;
using Core.AssetManagement;
using Core.LevelManagement;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;
using VContainer.Unity;
using Environment = Core.LevelManagement.Environment;

namespace Core.Application
{
    public class EnterPlayMode : IStartable // TODO Merge with ApplicationFSM
    {
        
        [Inject]
        private readonly IAssetResolver _assetResolver;
        
        [Inject]
        private readonly LifetimeScope _parentScope;

        [Inject] 
        private readonly LevelManager _levelManager;
        
        public void Start()
        {
            var activeSceneName = SceneManager.GetActiveScene().name;
            var env = _assetResolver
                .LoadAssets<Environment>()
                .FirstOrDefault(e => e.SceneName == activeSceneName);
            
            if (env == null) Debug.LogError($"There is no environment for scene {activeSceneName}");
            _levelManager.InjectIntoGameObjects(env);
        }
    }
}