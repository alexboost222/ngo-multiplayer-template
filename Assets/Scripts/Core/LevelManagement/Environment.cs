using System;
using Core.Application;
using Core.Interfaces;
using NaughtyAttributes;
using UnityEngine;

namespace Core.LevelManagement
{
    [CreateAssetMenu]
    public class Environment : ScriptableObject
    {
        [field: SerializeField] 
        public ApplicationFSM.State State { get; private set; }
        
        [field: SerializeField, Scene] 
        public string SceneName { get; private set; }

        [field: SerializeField] 
        public SerializableInstaller Installer { get; private set; }
    }
}