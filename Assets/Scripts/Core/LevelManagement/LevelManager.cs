using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;
using VContainer.Unity;

namespace Core.LevelManagement
{
    public class LevelManager
    {
        [Inject]
        private readonly LifetimeScope _projectLifetimeScope;

        private LifetimeScope _generatedChildScope;

        public async UniTask LoadEnv(Environment environment)
        {
            if (_generatedChildScope != null)
            {
                _generatedChildScope.Dispose();
                Object.Destroy(_generatedChildScope.gameObject);
            }
            
            await SceneManager.LoadSceneAsync(environment.SceneName);
            InjectIntoGameObjects(environment);
        }

        private LifetimeScope BuildLifetimeScope(Environment environment)
        {
            LifetimeScope scope;
            if (environment.Installer.Value != null)
            {
                _projectLifetimeScope.Container.Inject(environment.Installer.Value);
                scope = _projectLifetimeScope.CreateChild(environment.Installer.Value);
                scope.name = $"{environment.name}LifetimeScope";
                _generatedChildScope = scope;
            }
            else
                scope = _projectLifetimeScope;

            return scope;
        }

        public void InjectIntoGameObjects(Environment env)
        {
            var scope = BuildLifetimeScope(env);

            LifetimeScope.EnqueueParent(scope);
            var objs = Object.FindObjectsByType<GameObject>(FindObjectsSortMode.None);
            foreach (var obj in objs.Where(o => o.transform.parent == null)) scope.Container.InjectGameObject(obj);
        }
    }
}