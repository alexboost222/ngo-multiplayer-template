using System;
using System.Runtime.InteropServices;
using UnityEngine;


namespace Core.Input
{
    [RequireComponent(typeof(Canvas))]
    public class OnScreenInputCanvas : MonoBehaviour
    {
        
        
#if UNITY_WEBGL
        [DllImport("__Internal")]
        public static extern bool IsMobileBrowser();
#endif

        public static bool IsMobile()
        {
#if UNITY_EDITOR
            return false; // value to return in Play Mode (in the editor)
#elif UNITY_WEBGL
            return IsMobileBrowser(); // value based on the current browser
#else
            return UnityEngine.Application.platform switch
            {
                RuntimePlatform.Android => true,
                RuntimePlatform.IPhonePlayer => true,
                _ => false,
            }; // value for builds other than WebGL
#endif
        }



        private void Awake()
        {
            GetComponent<Canvas>().enabled = IsMobile();
        }
    }
}