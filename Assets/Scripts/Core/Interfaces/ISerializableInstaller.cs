using System;
using TNRD;
using VContainer.Unity;

namespace Core.Interfaces
{
    public interface ISerializableInstaller : IInstaller
    {
        
    }
    
    [Serializable]
    public class SerializableInstaller : SerializableInterface<ISerializableInstaller> {}
}