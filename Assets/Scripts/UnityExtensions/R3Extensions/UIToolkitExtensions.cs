using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using ObservableCollections;
using R3;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityExtensions
{
    public static class UIToolkitExtensions
    {
        public static IDisposable BindTo<T>(this ListView listView, ObservableList<T> entry,
            Action<VisualElement, T> elementBinding) => BindTo(entry, listView, elementBinding);

        public static IDisposable BindTo<T>(this ObservableList<T> entry, ListView listView, Action<VisualElement, T> elementBinding)
        {
            var disposableBag = new DisposableBag();
            var collection = entry.ToList();
            
            entry.ObserveAdd().Subscribe(e =>
            {
                collection.Insert(e.Index, e.Value);
                listView.RefreshItems();
            }).AddTo(ref disposableBag);
            
            entry.ObserveRemove().Subscribe(e =>
            {
                collection.RemoveAt(e.Index);
                listView.RefreshItems();
            }).AddTo(ref disposableBag);
            
            entry.ObserveReplace().Subscribe(e =>
            {
                collection[e.Index] = e.NewValue;
                listView.RefreshItem(e.Index);
            }).AddTo(ref disposableBag);

            entry.ObserveReset().Subscribe(_ =>
            {
                collection.Clear();
                listView.RefreshItems();
            }).AddTo(ref disposableBag);
            
            
            listView.itemsSource = collection;
            listView.bindItem = (element, i) => elementBinding.Invoke(element, collection[i]);

            return disposableBag;
        }


        public static void AsyncClicked(this Button btn, Func<UniTask> factory) =>
            btn.clicked += () => 
                UniTask.Create(async () =>
                {
                    btn.SetEnabled(false);
                    await factory.Invoke();
                    btn.SetEnabled(true);
                }).Forget();

        public static IDisposable AsyncClicked(this Button btn, Func<UniTask<Result>> factory)
        {
            var disposableBag = new DisposableBag();
            btn
                .OnClickAsObservable()
                .Subscribe(_ =>
                {
                    UniTask.Create(async () =>
                    {
                        btn.SetEnabled(false);
                        var result = await factory.Invoke();
                        if (result.IsError) Debug.LogError(result.Error);
                        btn.SetEnabled(true);
                    }).Forget();
                })
                .AddTo(ref disposableBag);

            return disposableBag;
        }


        public static Observable<Unit> OnClickAsObservable(this Button btn)
        {
            return Observable.FromEvent(
                add => btn.clicked += add,
                remove => btn.clicked -= remove);
        }

        public static IDisposable BindTo(this Label label, ReactiveProperty<string> prop)
        {
            var disposableBag = new DisposableBag();
            prop.Subscribe(val => label.text = val).AddTo(ref disposableBag);
            
            return disposableBag;
        }

        public static IDisposable VisibleBindTo(this VisualElement element, Observable<bool> property)
        {
            var disposableBag = new DisposableBag();
            property.Subscribe(val => element.visible = val).AddTo(ref disposableBag);
            return disposableBag;
        }

        public static bool All(this IEnumerable<bool> collection) => collection.All(a => a);
    }
}