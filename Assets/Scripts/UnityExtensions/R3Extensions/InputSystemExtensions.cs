using R3;
using UnityEngine.InputSystem;

namespace UnityExtensions.R3Extensions
{
    public static class InputSystemExtensions
    {
        public static Observable<T> AsOnUpdateObservable<T>(this InputAction inputAction) where T : struct
        {
            return Observable.EveryUpdate().Select(_ => inputAction.ReadValue<T>());
        }
        
        public static Observable<InputAction.CallbackContext> AsObservable(this InputAction inputAction)
        {
            return Observable.FromEvent<InputAction.CallbackContext>(
                    add => inputAction.performed += add,
                    remove => inputAction.performed -= remove);
        }
        
        public static Observable<T> AsObservable<T>(this InputAction inputAction) where T : struct
        {
            return inputAction
                .AsObservable()
                .Select(cc => cc.ReadValue<T>());
        }

        public static Observable<Unit> AsPerformedObservable(this InputAction inputAction)
        {
            return inputAction.AsObservable().Select(_ => Unit.Default);
        }
        
    }
}