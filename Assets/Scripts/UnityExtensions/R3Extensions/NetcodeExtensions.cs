using System;
using R3;
using Unity.Netcode;

namespace UnityExtensions.R3Extensions
{
    public static class NetcodeExtensions
    {
        public static Observable<(T oldValue, T newValue)> AsObservable<T>(this NetworkVariable<T> variable)
        {
            return Observable.FromEvent<NetworkVariable<T>.OnValueChangedDelegate, (T, T)>(
                action => (val, newVal) => action.Invoke((val, newVal)),
                add => variable.OnValueChanged += add,
                remove => variable.OnValueChanged += remove);
        }
    }
}