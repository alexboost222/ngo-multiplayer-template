using System;
using System.Collections.Generic;
using System.Linq;
using ObservableCollections;
using R3;
using R3.Triggers;
using TMPro;
using UnityEngine;

namespace UnityExtensions.R3Extensions
{
    public static class R3CustomExtensions
    {
        public static Observable<T> Debug<T>(this Observable<T> observable)
        {
            return observable.Select(v =>
            {
                UnityEngine.Debug.Log(v.ToString());
                return v;
            });
        }


        public static IDisposable Log<T>(this Observable<T> source)
        {
            return source.Subscribe(v => UnityEngine.Debug.Log(v));
        }

        public static Observable<T> SelectWhereNotNull<T, C>(this Observable<C> observable, Func<C, T> func)
        {
            return observable
                .Select(func)
                .Where(t => t != null);
        }
        
        public static Observable<IEnumerable<T>> SelectWhereNotNull<T, C>(this Observable<IEnumerable<C>> observable, Func<C, T> func)
        {
            return observable
                .Select(c => c.Select(func).Where(e => e != null));
        }


        public static IDisposable Subscribe<T>(this Observable<T> source, TMP_Text text)
        {
            return source.Subscribe(v => text.text = v.ToString());
        }


        public static Observable<IEnumerable<Collider>> OnCollidersInTrigger(
            this GameObject go,
            out DisposableBag disposableBag, 
            bool discardNulls = true)
        {
            var collection = new ObservableHashSet<Collider>();

            disposableBag = default;
            go
                .OnTriggerEnterAsObservable()
                .Subscribe(col => collection.Add(col))
                .AddTo(ref disposableBag);
            
            go
                .OnTriggerExitAsObservable()
                .Subscribe(col => collection.Remove(col))
                .AddTo(ref disposableBag);
            
            if (discardNulls)
                Observable.EveryUpdate()
                    .Subscribe(_ => collection.Remove(null))
                    .AddTo(ref disposableBag);
            
            return collection.ObserveCountChanged().Select(_ => collection as IEnumerable<Collider>);
        }
        
    }
}