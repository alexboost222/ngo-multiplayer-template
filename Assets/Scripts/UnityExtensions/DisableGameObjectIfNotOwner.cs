using Unity.Netcode;

namespace UnityExtensions
{
    public class DisableGameObjectIfNotOwner : NetworkBehaviour
    {
        public override void OnNetworkSpawn()
        {
            if (!IsOwner) gameObject.SetActive(false);
        }
    }
}