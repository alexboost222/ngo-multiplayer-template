using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityExtensions
{
    public static class RandomGenerator
    {
        public static string GetRandomFullName()
        {
            var adjectives = new List<string>()
            {
                "Brave",
                "Cunning",
                "Witty",
                "Majestic",
                "Swift",
                "Regal",
                "Fierce",
                "Noble",
                "Radiant",
                "Mystical",
            };

            var animals = new List<string>
            {
                "Wolf",
                "Falcon",
                "Lion",
                "Tiger",
                "Eagle",
                "Panther",
                "Bear",
                "Hawk",
                "Phoenix",
                "Dragon",
            };

            return $"{adjectives.PickRandom()} {animals.PickRandom()}";
        }


        public static string GetRandomLobbyName()
        {
            return new List<string>
            {

                "Dragon's Lair",
                "Galaxy Conquest",
                "Pirate's Cove",
                "Mystic Meadows",
                "Cyber Arena",
                "Zombie Apocalypse",
                "Ancient Ruins",
                "Space Odyssey",
                "Ninja Showdown",
                "Wild West Duel",
                "Fantasy Quest",
                "Underwater Odyssey",
                "Superhero Showdown",
                "Robot Rampage",
                "Magic Mayhem",
            }.PickRandom();
        }

        public static T PickRandom<T>(this List<T> collection)
        {
            return collection[Random.Range(0, collection.Count)];
        }
        
        public static T PickRandom<T>(this IEnumerable<T> collection)
        {
            return collection.Skip(Random.Range(0, collection.Count())).First();
        }
    }
}