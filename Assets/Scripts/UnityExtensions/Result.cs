using System;
using Cysharp.Threading.Tasks;
using R3;

namespace UnityExtensions
{
    public class Result<T>
    {
        public bool IsOk => Error == null;
        public bool IsError => !IsOk;
        
        public T Ok { get; private set; }
        public Exception Error { get; private set; }

        public Result(T ok) => Ok = Ok;

        public Result(Exception err) => Error = err;

        public Result<T> Wrap(Func<T> action)
        {
            try
            {
                return new Result<T>(action.Invoke());
            }
            catch (Exception e)
            {
                return new Result<T>(e);
            }
        }
    }


    public class Result : Result<Unit>
    {
        public Result() : base(Unit.Default)
        {
        }

        public Result(Exception err) : base(err)
        {
        }

        public static async UniTask<Result> Wrap(Func<UniTask> factory)
        {
            try
            {
                await factory.Invoke();
                return new Result();
            }
            catch (Exception e)
            {
                return new Result(e);
            }
        }
    }
    
}