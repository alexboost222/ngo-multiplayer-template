using System;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace UnityExtensions
{
    public abstract class ConfigSingleton<T> : ConfigSingletonBase where T : ConfigSingleton<T>
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetStatic()
        {
            _resourcesInstance = null;
        }
        
        private static T _resourcesInstance;

        public static T ResourcesInstance
        {
            get
            {
                if (_resourcesInstance == null)
                    _resourcesInstance = Resources.LoadAll<T>("Configs").FirstOrDefault();

                return _resourcesInstance;
            }
        }
    }

    public abstract class ConfigSingletonBase : ScriptableObject
    {
        
        
#if UNITY_EDITOR
        [MenuItem("Tools/Configs/Create all configs")]
        public static void UnsureAllConfigsCreated()
        {
            var baseType = typeof(ConfigSingletonBase);
            var inheritors = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => baseType.IsAssignableFrom(t) && t != baseType);;
            
            foreach (var inheritor in inheritors
                         .Where(i => !i.IsAbstract)
                         .Where(i => !AssetDatabase.AssetPathExists(GetPath(i))))
            {
                var so = CreateInstance(inheritor);
                AssetDatabase.CreateAsset(so, GetPath(inheritor));
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                
                Debug.Log($"Created Assets at path: {GetPath(inheritor)}");
            }
        }
        
#endif

        private static string GetPath(Type t) => $"Assets/Resources/Configs/{t.Name}.asset";
    }
}