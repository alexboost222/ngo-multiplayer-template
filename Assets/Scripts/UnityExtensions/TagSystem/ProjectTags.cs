using System;
using System.Linq;

namespace UnityExtensions.TagSystem
{
    public class ProjectTags : ConfigSingleton<ProjectTags>
    {
        [Serializable]
        public class TagNamespace
        {
            public string name;
            public string[] tags;
        }

        public TagNamespace[] namespaces;
        
        
        public static string[] GetTagsByNameSpace(string tagNamespace)
        {
            return ResourcesInstance.namespaces.FirstOrDefault(n => n.name == tagNamespace)?.tags;
        }
    }
}