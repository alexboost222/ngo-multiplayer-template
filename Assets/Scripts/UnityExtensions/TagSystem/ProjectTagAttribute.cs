using System;
using NaughtyAttributes;
using UnityEngine;

namespace UnityExtensions.TagSystem
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ProjectTagAttribute : DrawerAttribute
    {
        public string TagNamespace { get; }

        public ProjectTagAttribute(string tagNamespace)
        {
            TagNamespace = tagNamespace;
        }
    }
}