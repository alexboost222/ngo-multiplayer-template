using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.UnityExtensions
{
    public static class EnumEditorPrefs
    {
        public static T GetOrDefault<T>(T init = default) where T : struct, Enum
        {
#if UNITY_EDITOR
            var key = Key<T>(); 
            if (EditorPrefs.HasKey(key) && Enum.TryParse<T>(EditorPrefs.GetString(key), out var applicationState))
                return applicationState;
#endif
            return init;
        }

        private static string Key<T>() where T : Enum
        {
            return $"EnumEditorPrefs_{typeof(T).FullName}";
        }

        public static void Set<T>(T value) where T : struct, Enum
        {
#if UNITY_EDITOR
            var key = Key<T>(); 
            EditorPrefs.SetString(key, value.ToString());
#endif
        }
    }
}