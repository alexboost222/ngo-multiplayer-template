using System;
using R3;
using UnityEngine;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

namespace Plugins.Joystick_Pack.Scripts.Base
{
    [RequireComponent(typeof(Joystick))]
    public class InputSystemJoystickBinding : OnScreenControl
    {
        [InputControl(layout = "Vector2")]
        [SerializeField]
        private string customControlPath;

        private Joystick _joystick;

        protected override string controlPathInternal
        {
            get => customControlPath;
            set => customControlPath = value;
        }
        
        private void Awake()
        {
            _joystick = GetComponent<Joystick>();

            Observable.EveryUpdate()
                .Select(_ => _joystick.Direction)
                .Subscribe(SendValueToControl)
                .AddTo(this);
        }
    }
}


// [AddComponentMenu("Input/On-Screen Button")]
// public class OnScreenButton : OnScreenControl, IPointerDownHandler, IPointerUpHandler
// {
//     public void OnPointerUp(PointerEventData data)
//     {
//         SendValueToControl(0.0f);
//     }
//
//     public void OnPointerDown(PointerEventData data)
//     {
//         SendValueToControl(1.0f);
//     }
//
//     [InputControl(layout = "Button")]
//     [SerializeField]
//     private string m_ControlPath;
//
//     protected override string controlPathInternal
//     {
//         get => m_ControlPath;
//         set => m_ControlPath = value;
//     }
// }