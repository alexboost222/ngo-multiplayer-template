using System.Collections;
using System.Collections.Generic;

namespace FSM
{
    public class CreateListDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, List<TValue>> _data = new();

        public void Add(TKey key, params TValue[] value)
        {
            if (!_data.TryGetValue(key, out _)) _data[key] = new List<TValue>();
            _data[key].AddRange(value);
        }

        public List<TValue> GetValue(TKey key)
        {
            if (!_data.TryGetValue(key, out _)) _data[key] = new List<TValue>();
            return _data[key];
        }
    }
}