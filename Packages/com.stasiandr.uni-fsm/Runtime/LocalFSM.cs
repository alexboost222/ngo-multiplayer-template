using System;
using System.Collections.Generic;
using FSM;
using R3;
using UnityEngine;

public sealed class LocalFSM<TState, TCommand> 
    where TState : Enum
    where TCommand : Enum
{
    public ReadOnlyReactiveProperty<TState> CurrentState => _currentState;
    private ReactiveProperty<TState> _currentState;
        
    private readonly Dictionary<(TState, TCommand), TState> _transitionRoutes = new();
        
    private readonly CreateListDictionary<TState, Action> _onEnter = new();
    private readonly CreateListDictionary<(TState, TState), Action> _transitionsTable = new();
    private readonly CreateListDictionary<TState, Action> _onExit = new();
        
    private LogType _logType;

    public static LocalFSM<TState, TCommand> CreateInstance(TState state, LogType logType = LogType.Error)
    {
        if (logType >= LogType.Log) Debug.Log($"Initializing FSM with State({state})");
        return new LocalFSM<TState, TCommand>
        {
            _currentState = new ReactiveProperty<TState>(state),
            _logType = logType,
        };
    }

    public void PushCommand(TCommand cmd)
    {
        if (_transitionRoutes.TryGetValue((_currentState.Value, cmd), out var nextState))
        {
            if (_logType >= LogType.Log)
                Debug.Log($"State({_currentState.Value}) -- Command({cmd}) --> State({nextState})");
            
            _onExit.GetValue(_currentState.Value).ForEach(a => a.Invoke());
            _transitionsTable.GetValue((_currentState.Value, nextState)).ForEach(a => a.Invoke());
            _onEnter.GetValue(nextState).ForEach(a => a.Invoke());

            _currentState.Value = nextState;
        }
        else if (_logType >= LogType.Error)
        {
            Debug.LogError($"State({_currentState.Value}) -- Command({cmd}) -x-> ???");
        }
    }


    public LocalFSM<TState, TCommand> RegisterTransition(TState from, TState to, TCommand command)
    {
        if (_transitionRoutes.TryGetValue((from, command), out var nextState) && !nextState.Equals(to) &&
            _logType >= LogType.Error)
        {
            Debug.LogError($"Command({command}) from State({from}) already leads to State({nextState})");
        }
        
            
        _transitionRoutes[(from, command)] = to;
        return this;
    }

    public LocalFSM<TState, TCommand> OnTransition(TState from, TState to, params Action[] callbacks)
    {
        _transitionsTable.Add((from, to), callbacks);

        return this;
    }

    public LocalFSM<TState, TCommand> OnEnter(TState state, params Action[] callbacks)
    {
        _onEnter.Add(state, callbacks);
        return this;
    }

    public LocalFSM<TState, TCommand> OnExit(TState state, params Action[] callbacks)
    {
        _onExit.Add(state, callbacks);
        return this;
    }
}